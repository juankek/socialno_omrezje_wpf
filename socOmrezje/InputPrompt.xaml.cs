﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace socOmrezje {
    /// <summary>
    /// Interaction logic for InputPrompt.xaml
    /// </summary>
    public partial class InputPrompt : Window {

        private string gotInput, gotOldTxt;


        public InputPrompt(string input, string oldTxt) {
            InitializeComponent();
            gotInput = input;
            gotOldTxt = oldTxt;
            inputText.Text = "Input "+input+":";
            inputBox.Text = gotOldTxt;
            inputBox.SelectAll();
            inputBox.Focus();
        }

        public string Answer {
            get { return inputBox.Text; }
        }

        private void okBtn_Click(object sender, RoutedEventArgs e) {
            DialogResult = true;
        }

        private void cancelBtn_Click(object sender, RoutedEventArgs e) {
            this.Close();
        }

    }
}
