﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace socOmrezje
{
    public class ClassKomentar
    {
        public ClassKomentar() { }
        public ClassKomentar(string ime = null, string kommentar = null)
        {
            Ime = ime;
            Komentar = kommentar;
        }
        public string Ime { get; set; }
        public string Komentar { get; set; }
    }
}
