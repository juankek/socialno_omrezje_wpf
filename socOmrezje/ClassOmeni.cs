﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace socOmrezje
{
    public class ClassOmeni
    {
        public ClassOmeni() { }
        public ClassOmeni(string ime = "Moje ime", string priimek = "Moj priimek", string mail = "moj mail", string status = "moj mail", string oMeni = "nekaj o meni", string profilna = null, string ozadje = null)
        {
            this.Ime = ime;
            this.Priimek = priimek;
            this.Mail = mail;
            this.Status = status;
            this.OMeni = oMeni;
            this.Profilna = profilna;
            this.Ozadje = ozadje;
        }

        public string Ime { get; set; }
        public string Priimek { get; set; }

        public string Mail { get; set; }
        public string Status { get; set; }
        public string OMeni { get; set; }
        public string Profilna { get; set; }
        public string Ozadje { get; set; }
    }
}
