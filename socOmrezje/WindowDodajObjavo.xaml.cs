﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace socOmrezje {
    /// <summary>
    /// Interaction logic for WindowDodajObjavo.xaml
    /// </summary>
    /// 

    public partial class WindowDodajObjavo : Window {
        DirectoryInfo x = new DirectoryInfo( Directory.GetCurrentDirectory());

        /*
         
            DirectoryInfo info = new DirectoryInfo(Directory.GetCurrentDirectory());
            DirectoryInfo par = info.Parent;
            string path = Directory.GetCurrentDirectory();
            if (liked)
            {

                likeImage.Source = new BitmapImage(new Uri(par.Parent.FullName.ToString()+"\\icons\\icon_like.png"));
                liked = false;
            }
             
             */
        string[] razpolozenjeArr;
        string[] zasebnostArr =
        {
            "Public",
            "Friend Only",
            "Private"
        };
        string text = "";
        Uri file = null;
        List<ClassPrijatelj> prijatelji = new List<ClassPrijatelj>();
        Uri razpolozenje = null;
        string lokacija = "";
        string defaultSlika = "";
        string fi = null;
        int rndLikes = 0;
        bool defLiked = false;
        ClassObjava objavaResult;
        public WindowDodajObjavo(List<ClassPrijatelj> prijatelji) {
            InitializeComponent();
            Random newRnd = new Random();
            rndLikes = newRnd.Next(1, 100);
            //MessageBox.Show(x.Parent.FullName);
            razpolozenjeArr = new string[] {
                x.Parent.Parent.FullName + @"\icons\icon_happy.png",
                x.Parent.Parent.FullName + @"\icons\icon_sad.png",
                x.Parent.Parent.FullName + @"\icons\icon_unhappy.png",
                x.Parent.Parent.FullName + @"\icons\icon_bored.png"
            };
            defaultSlika = uploadImage.Source.ToString();
            prijateljiListBox.ItemsSource = prijatelji;
        }

        public WindowDodajObjavo(List<ClassPrijatelj> prijatelji, ClassObjava obj)
        {
            InitializeComponent();
            razpolozenjeArr = new string[] {
                x.Parent.Parent.FullName + @"\icons\icon_happy.png",
                x.Parent.Parent.FullName + @"\icons\icon_sad.png",
                x.Parent.Parent.FullName + @"\icons\icon_unhappy.png",
                x.Parent.Parent.FullName + @"\icons\icon_bored.png"
            };
            rndLikes = obj.Likes;
            defLiked = obj.Liked;
            prijateljiListBox.ItemsSource = prijatelji;
            if(obj.fileUri!=null)
            if (System.IO.Path.GetExtension(obj.fileUri)==".mp4" || System.IO.Path.GetExtension(obj.fileUri) == ".wmv"|| System.IO.Path.GetExtension(obj.fileUri) == ".flv")
            {
                uploadImage.Visibility = Visibility.Collapsed;
                uploadVideo.Source = new Uri(obj.fileUri);
                uploadVideo.Visibility = Visibility.Visible;
            }
            else
            {
                uploadImage.Visibility = Visibility.Visible;
                uploadImage.Source = new BitmapImage(new Uri(obj.fileUri));
                uploadVideo.Visibility = Visibility.Collapsed;
            }
            int temp = 0;
            foreach (string x in zasebnostArr)
            {
                if (x == obj.Zasebnost)
                    zasebnostComboBox.SelectedIndex = temp;
                temp++;
            }
            temp = 0;
            foreach(string x in razpolozenjeArr)
            {
                if(x == obj.Razpolozenje)
                {
                    razpolozenjeListBox.SelectedIndex = temp;
                }
                temp++;
            }
            dodajLokacijo.Text = obj.Lokacija;
            temp = 0;
            foreach (ClassPrijatelj p in obj.Prijatelji)
            {
                {
                    foreach (ClassPrijatelj x in prijatelji) {
                        if (x.ImePriimek == p.ImePriimek && x.DoB == p.DoB && x.Spol == p.Spol) {
                            //prijateljiListBox.SelectedIndex = temp;listBox.SelectedItems.Add( listBox.Items.GetItemAt(rowIndex) );
                            prijateljiListBox.SelectedItems.Add(prijateljiListBox.Items[temp]);
                        }
                    }
                }
                temp++;
            }
            dodajObavoText.Document.Blocks.Clear();
            if (obj.TextObjava != null)
                dodajObavoText.Document.Blocks.Add(new Paragraph(new Run(obj.TextObjava)));
        }



        private void dodajObjavoBtn_Click(object sender, RoutedEventArgs e) {
            text = new TextRange(dodajObavoText.Document.ContentStart, dodajObavoText.Document.ContentEnd).Text;
            if (string.IsNullOrWhiteSpace(text) && uploadImage.Source.ToString()==defaultSlika) {
                MessageBox.Show("Dodati morate vsaj eno sliko ali text", "Napaka");
                return;
            }
            if(dodajLokacijo.Text == "")
            {
                MessageBox.Show("Lokacija je obvezna!", "Napaka");
                return;
            }
            if(razpolozenjeListBox.SelectedIndex == -1)
            {
                MessageBox.Show("Razpoloženje je obvezno!", "Napaka");
                return;
            }
            if (zasebnostComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Zasebnost je obvezna!", "Napaka");
                return;
            }
            foreach(ClassPrijatelj item in prijateljiListBox.SelectedItems)
            {
                prijatelji.Add(item);
            }
            int x = razpolozenjeListBox.SelectedIndex;
            if (fi == null)
                objavaResult = new ClassObjava(text.TrimEnd('\r', '\n'), null, prijatelji, razpolozenjeArr[x], dodajLokacijo.Text, zasebnostArr[zasebnostComboBox.SelectedIndex],null , rndLikes, defLiked);
            else
            objavaResult = new ClassObjava(text.TrimEnd('\r', '\n'), fi, prijatelji, razpolozenjeArr[x], dodajLokacijo.Text , zasebnostArr[zasebnostComboBox.SelectedIndex],null ,rndLikes, defLiked);

            DialogResult = true;
        }

        private void prekliciObjavoBtn_Click(object sender, RoutedEventArgs e) {
            this.Close();
        }

        private void uploadFileBtn_Click(object sender, RoutedEventArgs e) {
            OpenFileDialog newFile = new OpenFileDialog();
            string folder = Directory.GetCurrentDirectory()+"\\Media\\";
            newFile.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG|Video Files(*.MP4;*.WMV;*.FLV)|*.MP4; *.WMV; *.FLV";
            if (newFile.ShowDialog() == true)
            {
                string newF = folder + newFile.SafeFileName;
                fi = newF;
                if (!File.Exists(folder + newFile.SafeFileName))
                {
                    File.Copy(newFile.FileName, newF, true);
                }
                if (System.IO.Path.GetExtension(newFile.FileName) == ".mp4" || System.IO.Path.GetExtension(newFile.FileName) == ".flv" || System.IO.Path.GetExtension(newFile.FileName) == ".wmv")
                //if (newFile.FileName.Contains(".FLV")|| newFile.FileName.Contains(".MP4") || newFile.FileName.Contains(".WMV"))
                {
                    //video
                    uploadImage.Visibility = Visibility.Collapsed;
                    uploadVideo.Source = new Uri(newF);
                    uploadVideo.Visibility = Visibility.Visible;
                }
                else
                {
                    //image
                    uploadImage.Visibility = Visibility.Visible;
                    uploadImage.Source = new BitmapImage(new Uri(newF));
                    uploadVideo.Visibility = Visibility.Collapsed;
                }
            }

        }

        public ClassObjava returnObjava
        {
            get { return objavaResult; }
        }
    }
}
