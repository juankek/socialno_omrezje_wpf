﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace socOmrezje {
    public class ClassObjava {
        //string ime;
        public ClassObjava() { }
        public ClassObjava(string textObjava = "", string fileURI = null, List<ClassPrijatelj> prijatelji = null, string razpol = null, string lokacija = "", string zasebnost = "", List<ClassKomentar> komentari = null, int likes = 0, bool liked = false) {
            this.TextObjava = textObjava;
            this.fileUri = fileURI;
            this.Prijatelji = prijatelji;
            this.Razpolozenje = razpol;
            this.Lokacija = lokacija;
            this.Zasebnost = zasebnost;
            this.Komentari = komentari;
            this.Likes = likes;
            this.Liked = liked;
        }
        public string TextObjava { get; set; }
        public string fileUri { get; set; }
        public List<ClassPrijatelj> Prijatelji { get; set; }
        public string Razpolozenje { get; set; }
        public string Lokacija { get; set; }
        public string Zasebnost { get; set; }
        public List<ClassKomentar> Komentari { get; set; }

        public int Likes { get; set; }
        public bool Liked { get; set; }
    }
}
