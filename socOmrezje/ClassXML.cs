﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace socOmrezje
{
   // [Serializable()]
    public class ClassXML// : ISerializable
    {
        public ClassOmeni OMeni { get; set; }
        public List<ClassObjava> Objave { get; set; }
        public List<ClassPrijatelj> Prijatelji { get; set; }
        
        public string uporabnik { get; set; }

        public ClassXML() { }
        public ClassXML(ClassOmeni omeni = null, List<ClassObjava> objave = null, List<ClassPrijatelj> prijatelji = null)
        {
            OMeni = omeni;
            Objave = objave;
            Prijatelji = prijatelji;
        }

        /*
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Objave", Objave);
            info.AddValue("Prijatelji", Prijatelji);
            info.AddValue("oMeni", OMeni);
        }
        public ClassXML(SerializationInfo info, StreamingContext context)
        {
            OMeni = (ClassOmeni)info.GetValue("oMeni", typeof(ClassOmeni));
            Objave = (List<ClassObjava>)info.GetValue("Objave", typeof(List<ClassObjava>));
            Prijatelji = (List<ClassPrijatelj>)info.GetValue("Prijatelji", typeof(List<ClassPrijatelj>));
        }*/
    }
}
