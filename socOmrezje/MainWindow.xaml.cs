﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;
using System.IO;
using Microsoft.Win32;
using System.Windows.Threading;
using System.Windows.Media.Animation;

namespace socOmrezje {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        private List<ClassPrijatelj> myFriends = new List<ClassPrijatelj>();
        private List<ClassObjava> objave = new List<ClassObjava>();
        private ClassOmeni jaz;
        private ClassXML mojXml;
        private Storyboard story = new Storyboard();
        private Storyboard story1 = new Storyboard();
        private Storyboard story2 = new Storyboard();
        private Storyboard story3 = new Storyboard();
        private int countMin = 0;
        private int cas;
        public MainWindow() {
            InitializeComponent();
            layoutChange(0);
            setAnimations();
            //prevzetiLayout.IsChecked = true;
            string pt = Directory.GetCurrentDirectory();
            //MessageBox.Show(pt);
            //objaveListBox.Items.Add(objave[0]);
            if (!Directory.Exists("Media")) {
                Directory.CreateDirectory("Media");
            }
            if (File.Exists("Podatki.xml") && new FileInfo("Podatki.xml").Length > 0) {
                XmlSerializer PodatkiSerializer = new XmlSerializer(typeof(ClassXML));
                using (StreamReader wr = new StreamReader("Podatki.xml")) {
                    mojXml = (ClassXML)PodatkiSerializer.Deserialize(wr);
                }
                if (mojXml.uporabnik != pt) {
                    //MessageBox.Show(mojXml.uporabnik);
                    mojXml = changeUser(mojXml, mojXml.uporabnik);
                }
                myFriends = mojXml.Prijatelji;
                objave = mojXml.Objave;
                if (mojXml.OMeni != null) {

                    jaz = mojXml.OMeni;

                    imeTextBlock.Text = jaz.Ime;
                    priimekTextBlock.Text = jaz.Priimek;
                    mailTextBlock.Text = jaz.Mail;
                    statusTextBlcok.Text = jaz.Status;
                    oMeniTextblock.Text = jaz.OMeni;
                    if (jaz.Profilna != null)
                        prikazna_slika.Source = new BitmapImage(new Uri(jaz.Profilna));
                    if (jaz.Ozadje != null)
                        ozadjeImg.Source = new BitmapImage(new Uri(jaz.Ozadje));
                } else {
                    jaz = new ClassOmeni();
                }
                foreach (ClassObjava x in objave) {
                    objaveListBox.Items.Add(new UserControlObjava(x, myFriends, jaz.Profilna));
                    if (x.fileUri != null) {//.BMP;*.JPG;*.GIF;*.PNG
                        if (System.IO.Path.GetExtension(x.fileUri) == ".png" || System.IO.Path.GetExtension(x.fileUri) == ".bmp" || System.IO.Path.GetExtension(x.fileUri) == ".gif" || System.IO.Path.GetExtension(x.fileUri) == ".jpg") {
                            Image newImg = new Image();
                            newImg.Source = new BitmapImage(new Uri(x.fileUri));
                            Style style = this.FindResource("galleryImg") as Style;
                            newImg.Style = style;
                            listBoxGalerija.Items.Add(newImg);
                        }
                    }
                }
            } else {
                File.Create("Podatki.xml");
                jaz = new ClassOmeni();
                objave = new List<ClassObjava>();
                myFriends = new List<ClassPrijatelj>();
            }
            prijateljiListBox.ItemsSource = myFriends;
            prijateljiComboBox.SelectedIndex = 0;
            cas = 2;
            DispatcherTimer dispatcherTimer = new DispatcherTimer();

            dispatcherTimer.Tick += new EventHandler(saveChanges);
            dispatcherTimer.Interval = new TimeSpan(0, 0, cas*60);
            dispatcherTimer.Start();

            DispatcherTimer opozoriloTimer = new DispatcherTimer();
            opozoriloTimer.Tick += new EventHandler(longUseWarning);
            opozoriloTimer.Interval = new TimeSpan(0, 0, cas * 60);
            opozoriloTimer.Start();
        }
        private void longUseWarning(object sender, EventArgs e)
        {
            countMin += cas;
            MessageBox.Show("Na socialnem omrežju si že " + countMin.ToString() + " minute, vzemi pavzo.", "Opozorilo");
        }



        private void saveChanges(object sender, EventArgs e) {
            //MessageBox.Show("SAVED");
            ClassXML temp = new ClassXML();
            //jaz.Profilna = prikazna_slika.Source.ToString();
            temp.Objave = objave;
            temp.OMeni = jaz;
            temp.Prijatelji = myFriends;
            temp.uporabnik = Directory.GetCurrentDirectory();
            XmlSerializer PodatkiSerializer = new XmlSerializer(typeof(ClassXML));
            using (TextWriter wr = new StreamWriter("Podatki.xml")) {
                PodatkiSerializer.Serialize(wr, temp);
            }
        }

        public ClassXML changeUser(ClassXML moj, string old) {

            DirectoryInfo fi = new DirectoryInfo(Directory.GetCurrentDirectory());
            string oldIcons = old.Replace("\\bin\\Debug", "");
            foreach (ClassObjava x in moj.Objave) {
                //MessageBox.Show(oldIcons, fi.Parent.Parent.FullName);
                string y = x.Razpolozenje.Replace(oldIcons, fi.Parent.Parent.FullName);
                //MessageBox.Show(y);
                x.Razpolozenje = y;
                if(x.fileUri!=null)
                x.fileUri = x.fileUri.Replace(old, Directory.GetCurrentDirectory());
            }
            foreach (ClassPrijatelj x in moj.Prijatelji) {
                x.Slika = x.Slika.Replace(old, Directory.GetCurrentDirectory());
            }

            moj.OMeni.Profilna = moj.OMeni.Profilna.Replace(old, Directory.GetCurrentDirectory());
            moj.OMeni.Ozadje = moj.OMeni.Ozadje.Replace(old, Directory.GetCurrentDirectory());
            moj.uporabnik = Directory.GetCurrentDirectory();
            return moj;
        }




        private void reloadObjave() {
            listBoxGalerija.Items.Clear();
            objaveListBox.Items.Clear();
            foreach (ClassObjava x in objave) {
                objaveListBox.Items.Add(new UserControlObjava(x, myFriends, jaz.Profilna));


                if (x.fileUri != null) {//.BMP;*.JPG;*.GIF;*.PNG
                    if (System.IO.Path.GetExtension(x.fileUri) == ".png" || System.IO.Path.GetExtension(x.fileUri) == ".bmp" || System.IO.Path.GetExtension(x.fileUri) == ".gif" || System.IO.Path.GetExtension(x.fileUri) == ".jpg") {
                        Image newImg = new Image();
                        newImg.Source = new BitmapImage(new Uri(x.fileUri));
                        listBoxGalerija.Items.Add(newImg);
                    }
                }

            }
        }

        private void Window_Closing(object sender, EventArgs e) {
            ClassXML temp = new ClassXML();
            //jaz.Profilna = prikazna_slika.Source.ToString();
            temp.Objave = objave;
            temp.OMeni = jaz;
            temp.Prijatelji = myFriends;
            temp.uporabnik = Directory.GetCurrentDirectory();
            XmlSerializer PodatkiSerializer = new XmlSerializer(typeof(ClassXML));
            using (TextWriter wr = new StreamWriter("Podatki.xml")) {
                PodatkiSerializer.Serialize(wr, temp);
            }
        }


        public void objavaSpremenjena(ClassObjava x, ClassObjava stara) {
            objave[objave.IndexOf(stara)] = x;

            //reloadObjave();

        }
        public void objavaZbrisi(ClassObjava stara) {
            objave.Remove(stara);
            reloadObjave();
        }
        /** IZHOD **/
        private void MenuItem_Click(object sender, RoutedEventArgs e) {
            this.Close();
        }
        /** DODAJ OBJAVO
         * TODO DODAJ Z POMOCJO USER CONTROLERJA**/
        private void MenuItem_Click_1(object sender, RoutedEventArgs e) {
            ClassObjava newOb;
            WindowDodajObjavo newObjava = new WindowDodajObjavo(myFriends);
            if (newObjava.ShowDialog() == true) {
                newOb = newObjava.returnObjava;
                objave.Insert(0,newOb);
                //objaveListBox.Items.Insert(0, new UserControlObjava(newOb, myFriends, jaz.Profilna));
                reloadObjave();
            }
        }
        /** SPREMINJANJE PODATKOV **/
        private void btnIme_Click(object sender, RoutedEventArgs e) {
            InputPrompt prompt = new InputPrompt("ime", imeTextBlock.Text);
            if (prompt.ShowDialog() == true) {
                imeTextBlock.Text = prompt.Answer;
                jaz.Ime = prompt.Answer;
            }
        }

        private void btnPriimek_Click(object sender, RoutedEventArgs e) {
            InputPrompt prompt = new InputPrompt("priimek", priimekTextBlock.Text);
            if (prompt.ShowDialog() == true) {
                priimekTextBlock.Text = prompt.Answer;
                jaz.Priimek = prompt.Answer;
            }
        }

        private void btnMail_Click(object sender, RoutedEventArgs e) {
            InputPrompt prompt = new InputPrompt("E-mail", mailTextBlock.Text);
            if (prompt.ShowDialog() == true) {
                mailTextBlock.Text = prompt.Answer;
                jaz.Mail = prompt.Answer;
            }
        }

        private void btnStatus_Click(object sender, RoutedEventArgs e) {
            InputPrompt prompt = new InputPrompt("status", statusTextBlcok.Text);
            if (prompt.ShowDialog() == true) {
                statusTextBlcok.Text = prompt.Answer;
                jaz.Status = prompt.Answer;
            }
        }

        private void btnOMeni_Click(object sender, RoutedEventArgs e) {
            InputPrompt prompt = new InputPrompt("o meni", oMeniTextblock.Text);
            if (prompt.ShowDialog() == true) {
                oMeniTextblock.Text = prompt.Answer;
                jaz.OMeni = prompt.Answer;
            }
        }

        private void prijateljiComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            sortFriends();
        }

        private void sortFriends() {
            switch (prijateljiComboBox.SelectedIndex) {
                case 0:
                    Comparison<ClassPrijatelj> comparison = (x, y) => String.Compare(x.ImePriimek, y.ImePriimek);
                    myFriends.Sort(comparison);
                    prijateljiListBox.Items.Refresh();
                    break;
                case 1:
                    Comparison<ClassPrijatelj> comparison2 = (x, y) => String.Compare(y.ImePriimek, x.ImePriimek);
                    myFriends.Sort(comparison2);
                    prijateljiListBox.Items.Refresh();
                    break;
                default:
                    break;
            }
        }


        private void prijateljiDodajButton_Click(object sender, RoutedEventArgs e) {
            ClassPrijatelj tempP;
            WindowDodajPrijatelja newPrijatelj = new WindowDodajPrijatelja();
            if (newPrijatelj.ShowDialog() == true) {
                tempP = newPrijatelj.NovPrijatel;
                myFriends.Add(tempP);
                sortFriends();
                prijateljiListBox.Items.Refresh();
            }
        }

        private void prijateljiIzbrisiButton_Click(object sender, RoutedEventArgs e) {
            if (prijateljiListBox.SelectedIndex == -1) {
                MessageBox.Show("Prvi izberi prijatelja!", "NAPAKA");
                return;
            }
            int x = prijateljiListBox.SelectedIndex;
            myFriends.RemoveAt(x);
            prijateljiListBox.Items.Refresh();
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e) {
            ClassXML temp = new ClassXML();
            //jaz.Profilna = prikazna_slika.Source.ToString();
            temp.Objave = objave;
            temp.OMeni = jaz;
            temp.Prijatelji = myFriends;
            temp.uporabnik = Directory.GetCurrentDirectory();
            XmlSerializer PodatkiSerializer = new XmlSerializer(typeof(ClassXML));
            using (TextWriter wr = new StreamWriter("Podatki.xml")) {
                PodatkiSerializer.Serialize(wr, temp);
            }
            /*

            try
            {
                mojXml.prijatelji = myFriends;
                mojXml.objave = objave;
                mojXml.jaz = jaz;
                XmlSerializer PodatkiSerializer = new XmlSerializer(typeof(ClassXML));
                using (TextWriter wr = new StreamWriter("Podatki.xml"))
                {
                    PodatkiSerializer.Serialize(wr, mojXml);
                }
            }
            catch (Exception et)
            {
                MessageBox.Show(et.ToString());
            }*/
        }

        private void updateProfilnoBtn_Click(object sender, RoutedEventArgs e) {
            OpenFileDialog newFile = new OpenFileDialog();
            string folder = Directory.GetCurrentDirectory() + "\\Media\\";
            newFile.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG";
            if (newFile.ShowDialog() == true) {
                string newF = folder + newFile.SafeFileName;
                if (!File.Exists(newF)) {
                    File.Copy(newFile.FileName, newF, true);
                }
                jaz.Profilna = newF;
                prikazna_slika.Source = new BitmapImage(new Uri(jaz.Profilna));
            }
        }

        private void changeOzadjeBtn_Click(object sender, RoutedEventArgs e) {
            OpenFileDialog newFile = new OpenFileDialog();
            string folder = Directory.GetCurrentDirectory() + "\\Media\\";
            newFile.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG";
            if (newFile.ShowDialog() == true) {
                string newF = folder + newFile.SafeFileName;
                if (!File.Exists(newF)) {
                    File.Copy(newFile.FileName, newF, true);
                }
                jaz.Ozadje = newF;
                ozadjeImg.Source = new BitmapImage(new Uri(jaz.Ozadje));
            }
        }

        public void izvozClick(object sender, RoutedEventArgs e) {
            /*if (File.Exists("Podatki.xml"))
                MessageBox.Show("OBSTAJA");*/
            saveChanges(sender, e);
            Microsoft.Win32.SaveFileDialog export = new Microsoft.Win32.SaveFileDialog();
            export.FileName = "Podatki";
            export.DefaultExt = ".xml";
            export.Filter = "XML (.xml)|*.xml";
            Nullable<bool> result = export.ShowDialog();
            if (result == true) {
                File.Copy("Podatki.xml", export.FileName, true);

            }
        }

        public void UvoziPodatke(object sender, RoutedEventArgs e) {
            OpenFileDialog newFile = new OpenFileDialog();
            newFile.Filter = "XML (.xml)|*.xml";
            Nullable<bool> result = newFile.ShowDialog();
            if (result == true) {
                File.Copy(newFile.FileName, "Podatki.xml", true);
                XmlSerializer PodatkiSerializer = new XmlSerializer(typeof(ClassXML));
                using (StreamReader wr = new StreamReader("Podatki.xml")) {
                    mojXml = (ClassXML)PodatkiSerializer.Deserialize(wr);
                }
                myFriends = mojXml.Prijatelji;
                prijateljiListBox.ItemsSource = myFriends;
                string pt = Directory.GetCurrentDirectory();
                    if (mojXml.uporabnik != pt) {
                        //MessageBox.Show(mojXml.uporabnik);
                        mojXml = changeUser(mojXml, mojXml.uporabnik);
                    }
                    objave = mojXml.Objave;
                    if (mojXml.OMeni != null) {
                        jaz = mojXml.OMeni;

                        imeTextBlock.Text = jaz.Ime;
                        priimekTextBlock.Text = jaz.Priimek;
                        mailTextBlock.Text = jaz.Mail;
                        statusTextBlcok.Text = jaz.Status;
                        oMeniTextblock.Text = jaz.OMeni;
                        if (jaz.Profilna != null)
                            prikazna_slika.Source = new BitmapImage(new Uri(jaz.Profilna));
                        if (jaz.Ozadje != null)
                            ozadjeImg.Source = new BitmapImage(new Uri(jaz.Ozadje));
                    } else {
                        jaz = new ClassOmeni();
                    }
                objaveListBox.Items.Clear();
                    foreach (ClassObjava x in objave) {
                        objaveListBox.Items.Add(new UserControlObjava(x, myFriends, jaz.Profilna));
                        if (x.fileUri != null) {//.BMP;*.JPG;*.GIF;*.PNG
                            if (System.IO.Path.GetExtension(x.fileUri) == ".png" || System.IO.Path.GetExtension(x.fileUri) == ".bmp" || System.IO.Path.GetExtension(x.fileUri) == ".gif" || System.IO.Path.GetExtension(x.fileUri) == ".jpg") {
                                Image newImg = new Image();
                                newImg.Source = new BitmapImage(new Uri(x.fileUri));
                                Style style = this.FindResource("galleryImg") as Style;
                                newImg.Style = style;
                                listBoxGalerija.Items.Add(newImg);
                            }
                        }
                    }
           
            }

        }

        private void prijateljiTextBox_TextChanged(object sender, TextChangedEventArgs e) {
            List<ClassPrijatelj> temp = new List<ClassPrijatelj>();
            if (prijateljiTextBox.Text == "") {
                prijateljiListBox.ItemsSource = myFriends;

            } else {

                foreach (ClassPrijatelj x in myFriends) {
                    if (x.ImePriimek.Contains(prijateljiTextBox.Text)){
                        temp.Add(x);
                    }
                }
                prijateljiListBox.ItemsSource = temp;
            }

        }


        private void setLayoutPrevzeto(object sender, RoutedEventArgs e) {
            layoutChange(0);

        }
        private void setLayoutAlt(object sender, RoutedEventArgs e) {
            layoutChange(1);
        }

        private void layoutChange(int x) {
            if (x == 1) {
                altLayour.IsChecked = true;
                prevzetiLayout.IsChecked = false;
                prikazna_slika.SetValue(Grid.RowProperty, 2);
                prikazna_slika.SetValue(Grid.ColumnProperty, 0);
                //profilanWrapPanel.SetValue(Grid.ColumnProperty, 0);
                //profilanWrapPanel.SetValue(Grid.RowProperty, 2);
                mainGrid.RowDefinitions[1].Height = new GridLength(0.1, GridUnitType.Star);
            } else {
                prevzetiLayout.IsChecked = true;
                altLayour.IsChecked = false;
                prikazna_slika.SetValue(Grid.RowProperty, 1);
                prikazna_slika.SetValue(Grid.ColumnProperty, 1);
                //profilanWrapPanel.SetValue(Grid.ColumnProperty, 1);
                //profilanWrapPanel.SetValue(Grid.RowProperty, 1);
                mainGrid.RowDefinitions[1].Height = new GridLength(0.3, GridUnitType.Star);
            }
        }


        private void setAnimations()
        {
            StringAnimationUsingKeyFrames ani = new StringAnimationUsingKeyFrames();
            ani.Duration = new Duration(new TimeSpan(0, 0, 5));
            ani.FillBehavior = FillBehavior.HoldEnd;
            ani.KeyFrames.Add(new DiscreteStringKeyFrame("Z", TimeSpan.FromSeconds(0.5)));
            ani.KeyFrames.Add(new DiscreteStringKeyFrame("Zi", TimeSpan.FromSeconds(1)));
            ani.KeyFrames.Add(new DiscreteStringKeyFrame("Zid", TimeSpan.FromSeconds(1.5)));
            Storyboard.SetTargetName(ani, zidTextBlock.Name);
            Storyboard.SetTargetProperty(ani, new PropertyPath(Label.ContentProperty));
            story.Children.Add(ani);



            StringAnimationUsingKeyFrames ani1 = new StringAnimationUsingKeyFrames();
            ani1.Duration = new Duration(new TimeSpan(0, 0, 5));
            ani1.FillBehavior = FillBehavior.HoldEnd;
            ani1.KeyFrames.Add(new DiscreteStringKeyFrame("O", TimeSpan.FromSeconds(0.5)));
            ani1.KeyFrames.Add(new DiscreteStringKeyFrame("O ", TimeSpan.FromSeconds(1)));
            ani1.KeyFrames.Add(new DiscreteStringKeyFrame("O m", TimeSpan.FromSeconds(1.5)));
            ani1.KeyFrames.Add(new DiscreteStringKeyFrame("O me", TimeSpan.FromSeconds(2)));
            ani1.KeyFrames.Add(new DiscreteStringKeyFrame("O men", TimeSpan.FromSeconds(2.5)));
            ani1.KeyFrames.Add(new DiscreteStringKeyFrame("O meni", TimeSpan.FromSeconds(3)));
            Storyboard.SetTargetName(ani1, oMeniLabel.Name);
            Storyboard.SetTargetProperty(ani1, new PropertyPath(Label.ContentProperty));
            story1.Children.Add(ani1);


            StringAnimationUsingKeyFrames ani2 = new StringAnimationUsingKeyFrames();
            ani2.Duration = new Duration(new TimeSpan(0, 0, 5));
            ani2.FillBehavior = FillBehavior.HoldEnd;
            ani2.KeyFrames.Add(new DiscreteStringKeyFrame("P", TimeSpan.FromSeconds(0.5)));
            ani2.KeyFrames.Add(new DiscreteStringKeyFrame("Pr", TimeSpan.FromSeconds(1)));
            ani2.KeyFrames.Add(new DiscreteStringKeyFrame("Pri", TimeSpan.FromSeconds(1.5)));
            ani2.KeyFrames.Add(new DiscreteStringKeyFrame("Prij", TimeSpan.FromSeconds(2)));
            ani2.KeyFrames.Add(new DiscreteStringKeyFrame("Prija", TimeSpan.FromSeconds(2.5)));
            ani2.KeyFrames.Add(new DiscreteStringKeyFrame("Prijat", TimeSpan.FromSeconds(3)));
            ani2.KeyFrames.Add(new DiscreteStringKeyFrame("Prijate", TimeSpan.FromSeconds(3.5)));
            ani2.KeyFrames.Add(new DiscreteStringKeyFrame("Prijatel", TimeSpan.FromSeconds(4)));
            ani2.KeyFrames.Add(new DiscreteStringKeyFrame("Prijatelj", TimeSpan.FromSeconds(4.5)));
            ani2.KeyFrames.Add(new DiscreteStringKeyFrame("Prijatelji", TimeSpan.FromSeconds(5)));
            Storyboard.SetTargetName(ani2, textPrijatelji.Name);
            Storyboard.SetTargetProperty(ani2, new PropertyPath(Label.ContentProperty));
            story2.Children.Add(ani2);

            StringAnimationUsingKeyFrames ani3 = new StringAnimationUsingKeyFrames();
            ani3.Duration = new Duration(new TimeSpan(0, 0, 5));
            ani3.FillBehavior = FillBehavior.HoldEnd;
            ani3.KeyFrames.Add(new DiscreteStringKeyFrame("S", TimeSpan.FromSeconds(0.5)));
            ani3.KeyFrames.Add(new DiscreteStringKeyFrame("Sl", TimeSpan.FromSeconds(1)));
            ani3.KeyFrames.Add(new DiscreteStringKeyFrame("Sli", TimeSpan.FromSeconds(1.5)));
            ani3.KeyFrames.Add(new DiscreteStringKeyFrame("Slik", TimeSpan.FromSeconds(2)));
            ani3.KeyFrames.Add(new DiscreteStringKeyFrame("Slike", TimeSpan.FromSeconds(2.5)));
            Storyboard.SetTargetName(ani3, textSlikeLabl.Name);
            Storyboard.SetTargetProperty(ani3, new PropertyPath(Label.ContentProperty));
            story3.Children.Add(ani3);
        }

        private void zidTextBlock_MouseEnter(object sender, MouseEventArgs e)
        {
            story.Begin(this);
        }

        private void oMeniTextBlock_MouseEnter(object sender, MouseEventArgs e)
        {
            story1.Begin(this);
        }
        private void textPrijatelji_MouseEnter(object sender, MouseEventArgs e)
        {
            story2.Begin(this);
        }
        private void textSlikeLabl_MouseEnter(object sender, MouseEventArgs e)
        {
            story3.Begin(this);
        }


    }
}
