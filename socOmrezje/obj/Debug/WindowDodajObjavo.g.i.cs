﻿#pragma checksum "..\..\WindowDodajObjavo.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "E98C3FFE94A253885EBF081D10A06B9EAD79AE0958387635257F98E42FF6D51F"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using socOmrezje;


namespace socOmrezje {
    
    
    /// <summary>
    /// WindowDodajObjavo
    /// </summary>
    public partial class WindowDodajObjavo : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 30 "..\..\WindowDodajObjavo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RichTextBox dodajObavoText;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\WindowDodajObjavo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox zasebnostComboBox;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\WindowDodajObjavo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button dodajObjavoBtn;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\WindowDodajObjavo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button prekliciObjavoBtn;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\WindowDodajObjavo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image uploadImage;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\WindowDodajObjavo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MediaElement uploadVideo;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\WindowDodajObjavo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button uploadFileBtn;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\WindowDodajObjavo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox prijateljiListBox;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\WindowDodajObjavo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox razpolozenjeListBox;
        
        #line default
        #line hidden
        
        
        #line 134 "..\..\WindowDodajObjavo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox dodajLokacijo;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/socOmrezje;component/windowdodajobjavo.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\WindowDodajObjavo.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.dodajObavoText = ((System.Windows.Controls.RichTextBox)(target));
            return;
            case 2:
            this.zasebnostComboBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 3:
            this.dodajObjavoBtn = ((System.Windows.Controls.Button)(target));
            
            #line 42 "..\..\WindowDodajObjavo.xaml"
            this.dodajObjavoBtn.Click += new System.Windows.RoutedEventHandler(this.dodajObjavoBtn_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.prekliciObjavoBtn = ((System.Windows.Controls.Button)(target));
            
            #line 49 "..\..\WindowDodajObjavo.xaml"
            this.prekliciObjavoBtn.Click += new System.Windows.RoutedEventHandler(this.prekliciObjavoBtn_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.uploadImage = ((System.Windows.Controls.Image)(target));
            return;
            case 6:
            this.uploadVideo = ((System.Windows.Controls.MediaElement)(target));
            return;
            case 7:
            this.uploadFileBtn = ((System.Windows.Controls.Button)(target));
            
            #line 61 "..\..\WindowDodajObjavo.xaml"
            this.uploadFileBtn.Click += new System.Windows.RoutedEventHandler(this.uploadFileBtn_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.prijateljiListBox = ((System.Windows.Controls.ListBox)(target));
            return;
            case 9:
            this.razpolozenjeListBox = ((System.Windows.Controls.ListBox)(target));
            return;
            case 10:
            this.dodajLokacijo = ((System.Windows.Controls.TextBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

