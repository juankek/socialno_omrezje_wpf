﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace socOmrezje {
    public class ClassPrijatelj {
        public ClassPrijatelj() { }
        public ClassPrijatelj(string imepriimek, string prijateljSlika, string dob, string spol) {
            this.ImePriimek = imepriimek;
            this.Slika = prijateljSlika;
            this.DoB = dob;
            this.Spol = spol;
        }

        public string ImePriimek {get; set;
        }
        public string Slika {
            get; set;
        }
        public string DoB {
            get; set;
        }
        public string Spol {
            get; set;
        }


    }
}
