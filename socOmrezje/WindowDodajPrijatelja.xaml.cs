﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace socOmrezje {
    /// <summary>
    /// Interaction logic for WindowDodajPrijatelja.xaml
    /// </summary>
    public partial class WindowDodajPrijatelja : Window {
        string spol;
        string ime;
        string dob;
        string default_slika;
        Uri fileLink;
        ClassPrijatelj novPrijatelj;
        public WindowDodajPrijatelja() {
            InitializeComponent();
            spol = null;
            ime = null;
            dob = null;
            default_slika = newFriendImg.Source.ToString();
            fileLink = null;

        }

        private void dodajPrijateljaBtn_Click(object sender, RoutedEventArgs e) {
            if (noviPrijateljImeTextbox.Text == "")
            {
                MessageBox.Show("Ime je obvezno", "NAPAKA");
                return;
            }
            if (datumRojstvaPicker.Text == "")
            {
                MessageBox.Show("Datum rojstva je obvezen", "NAPAKA");
                return;
            }
            if (spol == null)
            {
                MessageBox.Show("Spol je obvezno", "NAPAKA");
                return;
            }
            if(newFriendImg.Source.ToString() == default_slika)
            {
                MessageBox.Show("Slike je obvezna", "NAPAKA");
                return;
            }
            ime = noviPrijateljImeTextbox.Text;
            dob = "Datum rojstva: "+ datumRojstvaPicker.Text;

            fileLink = new Uri( newFriendImg.Source.ToString());

            novPrijatelj = new ClassPrijatelj(ime, fileLink.ToString(), dob, "Spol: "+spol);

            DialogResult = true;
        }

        public ClassPrijatelj NovPrijatel {
            get { return novPrijatelj; }
        }

        private void radioButton_Checked(object sender, RoutedEventArgs e) {
            RadioButton ck = sender as RadioButton;
            if (ck.IsChecked.Value)
                spol = ck.Content.ToString();
        }

        private void prekliciPrijateljaBtn_Click(object sender, RoutedEventArgs e) {
            this.Close();
        }

        private void dodajPrijateljaSlikoBtn_Click(object sender, RoutedEventArgs e) {
            OpenFileDialog newFileDialog = new OpenFileDialog();
            string folder = Directory.GetCurrentDirectory() + "\\Media\\";
            newFileDialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG";
            if (newFileDialog.ShowDialog() == true)
            {
                string newF = folder + newFileDialog.SafeFileName;
                if (!File.Exists(folder + newFileDialog.SafeFileName))
                {
                    File.Copy(newFileDialog.FileName, newF, true);
                }
                newFriendImg.Source = new BitmapImage(new Uri(newF));  
            }  
        }
    }
}
