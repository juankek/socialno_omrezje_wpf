﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace socOmrezje {
    /// <summary>
    /// Interaction logic for UserControlObjava.xaml
    /// </summary>
    public partial class UserControlObjava : UserControl {
        bool liked = false;
        private ClassObjava classObjava;
        private ClassObjava staraObjava;
        string pfImg;
        List<ClassPrijatelj> prTemp;
        public UserControlObjava(ClassObjava objava, List<ClassPrijatelj>t, string profileImg) {
            InitializeComponent();
            prTemp = t;
            DirectoryInfo info = new DirectoryInfo(Directory.GetCurrentDirectory());
            DirectoryInfo par = info.Parent;
            pfImg = profileImg;
            staraObjava = objava;
            classObjava = objava;
            setValues(objava);
            if (classObjava.Liked == true) {
                likeImage.Source = new BitmapImage(new Uri(par.Parent.FullName.ToString() + "\\icons\\icon_liked.png"));
                likeNumberBtn.Text = (classObjava.Likes + 1).ToString();
            } else {
                likeNumberBtn.Text = classObjava.Likes.ToString();
            }

        }
        public void LikeChange() {

            DirectoryInfo info = new DirectoryInfo(Directory.GetCurrentDirectory());
            DirectoryInfo par = info.Parent;
            //DirectoryInfo par = info.Parent;
            if (classObjava.Liked == true) {
                likeNumberBtn.Text = classObjava.Likes.ToString();
                likeImage.Source = new BitmapImage(new Uri(par.Parent.FullName.ToString() + "\\icons\\icon_like.png"));
                classObjava.Liked = false;
            } else {
                likeImage.Source = new BitmapImage(new Uri(par.Parent.FullName.ToString() + "\\icons\\icon_liked.png"));
                likeNumberBtn.Text = (classObjava.Likes + 1).ToString();
                classObjava.Liked = true;
            }

            MainWindow t = (MainWindow)Window.GetWindow(this);
            t.objavaSpremenjena(classObjava, staraObjava);
        }
        private void setValues(ClassObjava objava)
        {
            if (commentListBox.Items.Count > 0)
                commentListBox.Visibility = Visibility.Visible;
            privacyBox.Text = objava.Zasebnost;
            objavaProfileImg.Source = new BitmapImage(new Uri(pfImg));
            objavaPosterText.Text = "Jan Gacnik at: " + objava.Lokacija;
            
            if (objava.TextObjava != null)
            {
                objavaText.Visibility = Visibility.Visible;
                objavaText.Text = objava.TextObjava;
            }
            if (objava.fileUri != null)
            {
                //MessageBox.Show(System.IO.Path.GetExtension(objava.fileUri));
                if(System.IO.Path.GetExtension(objava.fileUri)==".mp4"|| System.IO.Path.GetExtension(objava.fileUri) == ".flv" || System.IO.Path.GetExtension(objava.fileUri) == ".wmv")
                {

                    objavaImg.Visibility = Visibility.Collapsed;
                    objavaVideo.Visibility = Visibility.Visible;
                    objavaVideo.Source = new Uri(objava.fileUri);
                }
                else
                {

                    objavaImg.Visibility = Visibility.Visible;
                    objavaVideo.Visibility = Visibility.Collapsed;
                    objavaImg.Source = new BitmapImage(new Uri(objava.fileUri));
                }
                    
            }
            razpolozenjeImg.Source = new BitmapImage(new Uri(objava.Razpolozenje));
            if (objava.Komentari != null)
            {
                commentListBox.Items.Clear();
                commentListBox.Visibility = Visibility.Visible;
                foreach(ClassKomentar x in objava.Komentari)
                {
                    commentListBox.Items.Add(new UserControlKomentar(x));
                }
            }
            if (objava.Prijatelji.Count > 0) {
                string prt = null;
                foreach (ClassPrijatelj x in objava.Prijatelji) {
                    prt += x.ImePriimek + " & ";
                }
                prt = prt.Remove(prt.Length - 3);
                sPrijateljiTextBox.Text = "With : " + prt;
            }

        }

        private void urediObjavoBtn_Click(object sender, RoutedEventArgs e)
        {
            WindowDodajObjavo test = new WindowDodajObjavo(prTemp, classObjava);
            if(test.ShowDialog() == true)
            {
                classObjava = test.returnObjava;
                setValues(classObjava);
                MainWindow t = (MainWindow)Window.GetWindow(this);
                t.objavaSpremenjena(classObjava, staraObjava);
                staraObjava = classObjava;
            }
        }

        private void likeBtn_Click(object sender, RoutedEventArgs e)
        {

            DirectoryInfo info = new DirectoryInfo(Directory.GetCurrentDirectory());
            DirectoryInfo par = info.Parent;
            string path = Directory.GetCurrentDirectory();
            if (liked)
            {

                likeImage.Source = new BitmapImage(new Uri(par.Parent.FullName.ToString()+"\\icons\\icon_like.png"));
                liked = false;
            }
            else
            {
                likeImage.Source = new BitmapImage(new Uri(par.Parent.FullName.ToString() + "\\icons\\icon_liked.png"));
                liked = true;
            }
            LikeChange();
        }

        private void commentBtn_Click(object sender, RoutedEventArgs e)
        {
            commentStackPanel.Visibility = Visibility.Visible;
        }

        private void cancelCommentBtn_Click(object sender, RoutedEventArgs e)
        {
            commentTextBox.Clear();
            commentStackPanel.Visibility = Visibility.Collapsed;
        }

        private void postCommentBtn_Click(object sender, RoutedEventArgs e)
        {
            if(commentTextBox.Text != "")
            {

                ClassKomentar newComment = new ClassKomentar("Random", commentTextBox.Text);
                commentListBox.Items.Add(new UserControlKomentar(newComment));
                classObjava.Komentari.Add(newComment);
                MainWindow t = (MainWindow)Window.GetWindow(this);
                t.objavaSpremenjena(classObjava, staraObjava);

                setValues(classObjava);
            }
            else
            {
                MessageBox.Show("Komentar ne mora biti prazen!", "NAPAKA");
                return;
            }

        }

        private void odstraniObjavoBtn_Click(object sender, RoutedEventArgs e)
        {

            MainWindow t = (MainWindow)Window.GetWindow(this);
            t.objavaZbrisi(staraObjava);
        }
        private void test(object sender, RoutedEventArgs e)
        {
            FieldInfo hlp = typeof(MediaElement).GetField("_helper", BindingFlags.NonPublic | BindingFlags.Instance);
            object helperObject = hlp.GetValue(objavaVideo);
            FieldInfo stateField = helperObject.GetType().GetField("_currentState", BindingFlags.NonPublic | BindingFlags.Instance);
            MediaState state = (MediaState)stateField.GetValue(helperObject);
            objavaVideo.LoadedBehavior = MediaState.Manual;
            if(state == MediaState.Pause)
            {
                objavaVideo.Play();
            }
            else
            {

                objavaVideo.Pause();
            }
        }/*
        private void playVideo(object sender, RoutedEventArgs e)
        {
            objavaVideo.Source = new Uri(staraObjava.fileUri);
            objavaVideo.Play();
        }
        private void stopVideo(object sender, RoutedEventArgs e)
        {
            objavaVideo.Stop();
        }*/
    }
}
